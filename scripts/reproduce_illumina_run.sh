#!/bin/bash

help="Script to reproduce an illumina HiSeq run based on the timestamps of the original sequencing procedure. The .stats files of the original procedure are used as reference value. The time stamp to start the script should be a few minutes before the first time stamp of the .stats files.\n\nUSAGE:\n\treproduce_illumina_run.sh \$ORIG \$FROM \$TO \$MONTH \$DAY \$HOUR \$MINUTE\n\nPOSITIONAL PARAMETERS:\n\t\$ORIG\tPath to the original BaseCalls directory (written by the sequencing machine)\n\t\$FROM\tOrigin for base call file copying (equal to \$ORIG or a copy of it)\n\t\$TO\tTarget for base call file copying (input directory for the related PriLive run)\n\t\$MONTH\tMonth of the first time stamp\n\t\$DAY\tDay of the first time stamp\n\t\$HOUR\tHour of the first time stamp\n\t\$MINUTE\tAt least 1 minute before the first time stamp\n\nEXAMPLE:\n\treproduce_illumina_run.sh [illuminaPath]/BaseCalls [copyofIlluminaPath]/BaseCalls [PriLiveInput]/BaseCalls 8 4 15 24\nFor a sequencing run that started on 4th August at 3.24pm."

while getopts h opt
do
    case $opt in
        h) echo -e $help; exit 0;;
    esac
done

if [ "$#" -ne 7 ]; then
    echo "Wrong number of parameters. Type 'reproduce_illumina_run.sh -h' for help."
    exit 1;
fi

origPath=$1
fromPath=$2
toPath=$3
month=$4
day=$5
hours=$6
minutes=$7

monthHash=('' 'Jan' 'Feb' 'Mar' 'Apr' 'May' 'Jun' 'Jul' 'Aug' 'Sep' 'Okt' 'Nov' 'Dec')
ls_name="copy_list.txt"

ls -lRd -1 $origPath/**/**/* | grep "^-" | grep "stats" > ${toPath}/${ls_name}
num_files=$(wc -l < $toPath/$ls_name)
copied_files=0
dateString="";

echo "Starting copy process ..."
echo ""

while [[ $copied_files -lt $num_files ]]; do
    if [[ $minutes -ge 59 ]]; then
	if [[ $hours -ge 23 ]]; then
	    if [[ $day -ge 28 ]] && [[ $month -eq 2 ]]; then
		month=$(($month+1));
		day=1;
	    elif [[ $day -ge 30 ]] && ( [[ $month -eq 4 ]] || [[ $month -eq 6 ]] || [[ $month -eq 9 ]] || [[ $month -eq 11 ]] ); then
		month=$(($month+1));
		day=1;
	    elif [[ $day -ge 31 ]]; then
		month=$(($month+1));
		day=1;
	    else
		day=$(($day+1));
	    fi
	    hours=0;
	else
	    hours=$(($hours+1));
	fi
	minutes=0;
    else
	minutes=$(($minutes+1));
    fi

    if [[ $month -ge 13 ]]; then
	month=1;
    fi

    # Depending on the system setup,you may have to adapt this grep string.
    dateString="${monthHash[$month]}  $day $(printf %02d ${hours}):$(printf %02d $minutes)"
    
    files_to_copy=$(grep --no-group-separator "$dateString" ${toPath}/${ls_name})
    grep --no-group-separator "$dateString" ${toPath}/${ls_name} | while read -r line; do
								       file=$(awk -F ' ' '{print substr($9,2);}' <(echo $line))
								       lane=$(grep -oE "L[0-9]{1,3}" <(echo $file))
								       cycle=$(grep -oE "C[0-9]{1,3}.1" <(echo $file))
								       tile=$(grep -oE "s_[0-9]{1,3}_[0-9]{4}" <(echo $file))
								       mkdir -p $toPath/$lane/$cycle
								       cp -n ./$lane/* $toPath/$lane 2>/dev/null
								       cp -n ./$lane/$cycle/* $toPath/$lane/$cycle 2>/dev/null
								       cp $fromPath/$lane/$cycle/${tile}.* $toPath/$lane/$cycle 2>/dev/null
								   done &
    if [[ ! -z $files_to_copy ]]; then
	copied_files=$(($copied_files + $(echo "$files_to_copy" | wc -l | awk '{print $1}')))
    fi
    
    echo -en "\e[1A";
    echo -e "\e[0K\r $dateString"
    sleep 60s;
done
echo "Finished at `date`."
exit


