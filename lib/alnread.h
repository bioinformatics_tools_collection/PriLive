#ifndef ALNREAD_H
#define ALNREAD_H

#include "headers.h"
#include "definitions.h"
#include "kindex.h"
#include "tools.h"
#include <seqan/basic.h>
#include <seqan/bam_io.h>


//-------------------------------------------------------------------//
//------  The Seed data structure  ----------------------------------//
//-------------------------------------------------------------------//


/**
 * A Seed ( = alignment candidate ). Stores all relevant information of the alignment to a target position.
 * @author Martin Lindner
 */
struct Seed {

  /** Internal sequence ID of taget genome. */
  GenomeIdType gid;

  /** (Estimated) start position of the read on the target. */
  PositionType start_pos;

  /** Number of matching bases. */
  CountType num_matches;

  /** Information about matches/mismatches (similar to CIGAR). The last element is the current one. */
  CigarVector cigar_data;

  /**
   * Get the CIGAR string in SeqAn format.
   * @return Seqans String of CigarElement.
   * @author Jakob Schulze
   */
  seqan::String<seqan::CigarElement<> > returnSeqanCigarString();

  /**
   * Get the position where the last k-mer (should) has matched (for background alignment).
   * @param k_size Span of a k-mer (default: K_PriLive).
   * @return Position where the last k-mer (should) has matched.
   * @author Tobias Loka
   */
  PositionType getLastKmerPos(CountType k_size = K_PriLive);

  /**
   * Get the size of the serialized object.
   * @return Size of a serialized seed ( in bytes ).
   * @author Martin Lindner
   */
  uint16_t serialize_size();

  /**
   * Serialize the seed object.
   * @return Char vector containing the serialized seed.
   * @author Martin Lindner
   */
  std::vector<char> serialize();

  /**
   * Deserialize (seed) data from a char vector.
   * @param d Serialized seed data.
   * @return Size of deserialized data ( in bytes ).
   * @author Martin Lindner
   *
   */
  uint16_t deserialize(char* d);
};

/** Seed pointer object. */
typedef std::unique_ptr<Seed> USeed;

/**
 * Compare function to sort Seed objects by position ( and - secondary - by gid ).
 * @param i First seed.
 * @param j Second seed.
 * @return true, if i < j in terms of the defined comparison.
 * @author Martin Lindner, Tobias Loka
 */
bool seed_compare_pos (const USeed & i, const USeed & j);

/** Container to store all seeds for a single read. */
typedef std::list<USeed> SeedVec;

/** Iterator to iterate through a container of seeds. */
typedef SeedVec::iterator SeedVecIt;



//-------------------------------------------------------------------//
//------  The Read-Alignment class  ---------------------------------//
//-------------------------------------------------------------------//


class ReadAlignment {

 protected:

  /** Total number of cycles for this read ( = read length ). */
  CountType total_cycles;

  /** Length of the sequence determined so far. */
  CountType sequenceLen=0;

  /** Sequence of the read so far, saved as vector<uint8_t> so interpretation is not that trivial. */
  std::vector<uint8_t> sequenceStoreVector;

  /** Length of the barcode sequence(s) determined so far. */
  CountType barcodeLen=0;

  /** Barcode sequence(s) of the read so far, saved as vector<uint8_t> so interpretation is not that trivial. */
  std::vector<uint8_t> barcodeStoreVector;

  /**
   * Extend or create a placeholder seed for read with only trimmed matches.
   * @param settings The alignment settings.
   * @author Tobias Loka, Jakob Schulze
   */
  void create_placeholder_seed(AlignmentSettings & settings);

  /**
   * Convert a placeholder seed to a set of "normal" seeds.
   * @param pos List of positions to create "normal" seeds.
   * @author Tobias Loka, Jakob Schulze
   */
  void convertPlaceholder(GenomePosListType& pos, AlignmentSettings & settings);

  /**
   * Create new seeds from a list of kmer positions and add to current seeds.
   * @param pos List of positions to create seeds.
   * @param posWasUsedForExtension List of flags marking which positions have been used for seed extension (no seed creation for these positions).
   * @param settings The alignment settings.
   * @author Jakob Schulze
   */
  virtual void add_new_seeds(GenomePosListType& pos, std::vector<bool> & posWasUsedForExtension, AlignmentSettings & settings);

  /**
   * Filter seeds based on filtering mode and q gram lemma. Also calls add_new_seeds.
   * @param settings The alignment settings.
   * @param pos List of positions to create new seeds.
   * @param posWasUsedForExtension List of flags marking which positions have been used for seed extension (no seed creation for these positions).
   * @author Jakob Schulze
   */
  void filterAndCreateNewSeeds(AlignmentSettings & settings, GenomePosListType & pos, std::vector<bool> & posWasUsedForExtension);

  /**
   * Updates cigar_data accordingly to a new matching kmer.
   * @param s The seed to update the CIGAR data for.
   * @param offset The offset.
   * @param settings The alignment settings.
   * @author Jakob Schulze
   */
  void addMatchingKmer(USeed & s, DiffType offset, AlignmentSettings & settings);

  /**
   * Extend an existing CIGAR string for a seed based on a new basecall.
   * @param s The seed to extend.
   * @param offset The offset type ( match, no_match, .. ).
   * @param settings The alignment settings.
   * @return false if last CIGAR element after extension is mismatch area (NO_MATCH), true otherwise.
   * @author Tobias Loka, Jakob Schulze
   */
  bool extendSeed(USeed & s, DiffType offset, AlignmentSettings & settings);

 public:

  /** Flags for this read; 1 = read is valid (illumina flag). */
  unsigned char flags = 1;

  /** The last invalid cycle ( N-call or quality below threshold ). */
  CountType last_invalid;

  /** The current cycle. */
  CountType cycle;
  
  /** A list of all found seeds for the current read. */
  SeedVec seeds;

  /** Maximal number of matches for the current read. */
  CountType max_num_matches;

  /** Minimal number of errors for the current read. */
  CountType min_errors;

  /**
   * Set the total length of a read.
   * @param c Read length for the current read.
   */
  void set_total_cycles(CountType c);
  
  /**
   * Get the size of the serialized read data (in bytes)
   * @return Serialized size in bytes
   * @author Martin Lindner
   */
  uint64_t serialize_size();

  /**
   * Serialize (read) data to a char vector.
   * @return Char vector containing the serialized data ( as stored in the temporary files )
   * @author Martin Lindner
   */
  std::vector<char> serialize();

  /**
   * Deserialize (read) data from a char vector.
   * @param d Char vector containing the serialized read data ( as stored in the temporary files )
   * @return Number of bytes read.
   * @author Martin Lindner
   */
  uint64_t deserialize(char* d);

  /**
   * Get the sequence of a read as string object ( without barcode )
   * @return The Sequence as string
   * @author Jakob Schulze
   */
  std::string getSequenceString();

  /**
   * Convert and return sequence of the barcode. Multiple barcodes are concatenated (without delimiter).
   * @return The Barcode as string
   * @author Tobias Loka
   */
  std::string getBarcodeString();

  /**
     * Check whether the barcode of this read fulfills the criteria of at least one user-defined barcode.
     * The nucleotides are only compared pairwise, not allowing for Indels.
     * @param settings Object containing the program settings.
     * @return The index of the matching barcode in AlignmentSettings::multiBarcodeVector. NO_MATCH, if none.
     * Also return NO_MATCH, if demultiplexing is not activated.
     * @author 	Tobias Loka
     */
  CountType getBarcodeIndex(AlignmentSettings* settings) ;


  /**
   * Append one nucleotide to sequenceStoreVector
   * @param nucl The nucleotide. Must be 2-bit-formatted.
   * @param appendToBarcode If true, the nucleotide is appended to the barcode instead of the read sequence (default: false).
   * @return
   * @author Jakob Schulze
   */
  void appendNucleotideToSequenceStoreVector(char nuc, bool appendToBarcode=false);

  /**
   * Extend the alignment (i.e., the seeds) for a single read.
   * @param bc Base call (nucleotide + quality) for the current cycle.
   * @param index The k-mer reference index.
   * @param settings The alignment settings.
   * @author Martin Lindner, Jakob Schulze, Tobias Loka
   */
  virtual void extend_alignment(char bc, KixRun* index, AlignmentSettings* settings);

  /**
   * Minimal number of errors as needed for the background alignment.
   * Analyzes the full CIGAR vector.
   * @param s The respective seed.
   * @param settings The alignment settings.
   * @return Minimal number of errors for the seed.
   * @author Tobias Loka
   */
  CountType minErrors(USeed & s, AlignmentSettings* settings);

  /**
   * Disable the alignment for a single read such that it is handled no longer ( saves resources )
   * @author Martin Lindner
   */
  void disable();

  /**
   * Obtain SAM-specific ( leftmost ) start position of a seed.
   * @param sd The respective seed.
   * @param settings The alignment settings.
   * @return Alignment position as specified for SAM format.
   * @author Jakob Schulze
   */
  PositionType get_SAM_start_pos(USeed & sd, AlignmentSettings & settings);

  /**
   * Obtain SAM-specific alignment quality.
   * @param sd The respective seed.
   * @return Alignment quality as specified for SAM format.
   */
  uint16_t get_SAM_quality(USeed & sd);

  /**
   * Default destructor for ReadAlignment objects.
   */
  virtual ~ReadAlignment(){};

}; // END class ReadAlignment 


//-------------------------------------------------------------------//
//------  Other helper functions  -----------------------------------//
//-------------------------------------------------------------------//
int16_t MAPQ(const SeedVec &sv);

#endif /* ALNREAD_H */
