#include "tools.h"

// compares two genome positions by position (primary comparator) and gid (secondary comparatpr)
bool gp_compare (GenomePosType i,GenomePosType j) {
	if ( i.pos == j.pos )
		return i.gid < j.gid;
	return (i.pos < j.pos);
}

// reads a binary file from hdd and stores its raw content in a char vector
std::vector<char> read_binary_file(const std::string &fname) {
  
  // get file size
  uint64_t size = get_filesize(fname);

  // open binary file
  FILE* f;
  f = fopen(fname.c_str(), "rb");

//  if (!f) {
//    std::cerr << "Error reading binary file " << fname << ": Could not open file." << std::endl;
//    return std::vector<char>();
//  }

  if (!f) {
	  std::cerr << "Error reading binary file " << fname << ": Could not open file." << std::endl;
	  throw std::runtime_error("read binary file error (opening error)");
  }

  // allocate memory
  std::vector<char> data (size);
  
  // read all data at once
  uint64_t read = fread(data.data(), 1, size, f);

//  if (read != size){
//    std::cerr << "Error reading binary file " << fname << ": File size: " << size << " bytes. Read: " << read << " bytes." << std::endl;
//    return std::vector<char>();
//  }

  fclose(f);

  if (read != size){
    std::cerr << "Error reading binary file " << fname << ": File size: " << size << " bytes. Read: " << read << " bytes. Try again later." << std::endl;
	throw std::runtime_error("read binary file error (wrong size)");
  }

  return data;
}


// checks if a directory with the given path exists
bool is_directory(const std::string &path) {
  try {
	  if ( boost::filesystem::exists(path) ) {
		  if ( boost::filesystem::is_directory(path) ) {
			  return true;
		  }
		  else {
			  return false;
		  }
	  }
	  else {
		  return false;
	  }
  } catch (const std::exception &ex) {
	  return false;
  }
}

// checks if a file exists
bool file_exists(const std::string &fname) {
	try {
		return boost::filesystem::exists(fname);
	} catch (const std::exception &ex) {
		return false;
	}
  /*if (FILE *f = fopen(fname.c_str(), "r")) {
    fclose(f);
    return true;
  }
  else {
    return false;
    }*/ 
}


// writes a char vector into a binary file
uint64_t write_binary_file(const std::string &fname, const std::vector<char> & data) {

  // open binary file
  FILE* ofile;
  ofile = fopen(fname.c_str(), "wb");

  if (!ofile) {
    std::cerr << "Error serializing object to file " << fname << ": Could not open file for writing." << std::endl;
    throw std::runtime_error("Error during serialization: Could not write file " + fname);
  }

  // write all data
  uint64_t written = fwrite(data.data(), 1, data.size(), ofile);
  
  // close file
  fclose(ofile);

  if (written != data.size()){
    std::cerr << "Error serializing object to file " << fname << ": Total size: " << data.size() << " bytes. Written: " << written << " bytes." << std::endl;
    throw std::runtime_error("Error during serialization: Could not write file " + fname);
  }

  return written;
}

std::string getCurrentTimeString(){
	time_t rawtime;
	struct tm * timeinfo;

	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	std::string tm = asctime (timeinfo);
	tm.erase(std::remove(tm.begin(), tm.end(), '\n'), tm.end());
	return tm;
}

void consoleOut(std::string msg){
	std::cout << msg << std::endl;
}

void consoleOut(std::string msg, std::mutex* mtx){
	mtx->lock();
	std::cout << msg << std::endl;
	mtx->unlock();
}

void consoleErr(std::string msg){
	std::cerr << msg << std::endl;
}

void consoleErr(std::string msg, std::mutex* mtx){
	mtx->lock();
	std::cerr << msg << std::endl;
	mtx->unlock();
}

// extract the number of reads from a BCL file
uint32_t num_reads_from_bcl(std::string bcl) {
  // open BCL file of first cycle
  FILE* ifile;
  ifile = fopen(bcl.c_str(), "rb");

  if (!ifile) {
    std::cerr << "Error reading BCL file " << bcl << ": Could not open file." << std::endl;
    return 0;
  }

  // extract the number of reads
  uint32_t num_reads;
  assert(fread(&num_reads, 1, sizeof(uint32_t), ifile));

  // close file
  fclose (ifile);
  
  return num_reads;
}

/* get the size (in bytes) of a file */
std::ifstream::pos_type get_filesize(const std::string &fname)
{
  std::ifstream in(fname, std::ios::binary | std::ios::ate);
  return in.tellg(); 
}


/* calculates the first forward and reverse complement k-mer in the 
   string <kmer> and returns the canonical representation. */
HashIntoType hash(const char * kmer, HashIntoType& _h, HashIntoType& _r, AlignmentSettings & settings)
{
  assert(strlen(kmer) >= settings.kmer_span);

  HashIntoType h = 0, r = 0;

  h |= twobit_repr(kmer[0]);
  r |= twobit_comp(kmer[settings.kmer_span-1]);

  for (unsigned int i = 1, j = settings.kmer_span-2; i < settings.kmer_span; i++, j--) {
    // if i not gap position
    if (std::find(settings.kmer_gaps.begin(), settings.kmer_gaps.end(), i+1) == settings.kmer_gaps.end()) {
      h = h << 2;
      h |= twobit_repr(kmer[i]);
      r = r << 2;
      r |= twobit_comp(kmer[j]);
    }
  }

  _h = h;
  _r = r;

  return (h)<(r)?h:r;
}

/* calculates the first forward k-mer in the string <kmer> */
std::string::const_iterator hash_fw(std::string::const_iterator it, std::string::const_iterator end, HashIntoType& _h, AlignmentSettings & settings, bool gapped)
{
	unsigned kmer_span = gapped ? settings.kmer_span : K_PriLive;
	assert( it + kmer_span - 1 < end );
	HashIntoType h = 0;
	std::string::const_iterator last_invalid = it-1;

	h |= twobit_repr(*it);

	std::string::const_iterator kmerEnd = it + kmer_span;
	++it;
	int positionInKmer = 2;
	for (; it != kmerEnd; ++it, ++positionInKmer) {
		if ( gapped && std::find(settings.kmer_gaps.begin(), settings.kmer_gaps.end(), positionInKmer) != settings.kmer_gaps.end())
			continue;
		h = h << 2;
		h |= twobit_repr(*it);
		if ( seq_chars.find(*it) == std::string::npos ) {
			last_invalid = it + kmer_span - 1;
		}
	}

	_h = h;
	return last_invalid;
}


/* returns the sequence of a k-mer */
std::string unhash(HashIntoType myHash, unsigned hashLen)
{
	std::string kmer = "";

	unsigned mask = 3;
	for (unsigned i = 1; i<pow(2,2*hashLen); i *= 4) {
		kmer.push_back(revtwobit_repr(myHash & mask));
		myHash = myHash >> 2;
	}
	std::reverse(kmer.begin(), kmer.end());
	return kmer;
}

/* returns the reverse complement of a k-mer */
HashIntoType rc(HashIntoType fw)
{

  HashIntoType rc = 0;

  // Illumina uses
  // A = 0b00
  // C = 0b01
  // G = 0b10
  // T = 0b11
  // so, inverting bits produces the rc: rc(A) = ~A

  for (unsigned int i = 0; i < 2*K_PriLive; i+=2) {
    rc |= (~(fw >> i) & 3) << (2*K_PriLive - i - 2);
  }

  return rc;
}



// file name construction functions

// construct BCL file name from: root, lane, tile, cycle
std::string bcl_name(std::string rt, uint16_t ln, uint16_t tl, uint16_t cl) {
  std::ostringstream path_stream;
  path_stream << rt << "/L00" << ln << "/C" << cl << ".1/s_"<< ln <<"_" << tl << ".bcl";
  return path_stream.str();
}


// construct alignment file name from: root, lane, tile, cycle

std::string alignment_name(uint16_t ln, uint16_t tl, uint16_t cl, uint16_t mt, std::string base){
  std::ostringstream path_stream;
  path_stream << base << "/L00" << ln << "/s_"<< ln << "_" << tl << "." << mt << "."<< cl << ".align";
  return path_stream.str();
}

// construct tile-wise SAM file name from: root, lane, tile
std::string sam_tile_name(std::string rt, uint16_t ln, uint16_t tl, uint16_t mate, bool write_bam) {
  std::ostringstream path_stream;
  if (write_bam)
    path_stream << rt << "/L00" << ln << "/s_"<< ln << "_" << tl << "." << mate << ".bam";
  else
    path_stream << rt << "/L00" << ln << "/s_"<< ln << "_" << tl << "." << mate << ".sam";
  return path_stream.str();
}

// construct lane-wise SAM file name from: root, lane
std::string sam_lane_name(std::string rt, uint16_t ln, uint16_t mate, bool write_bam) {
  std::ostringstream path_stream;
  if (write_bam)
    path_stream << rt << "/L00" << ln << "/s_"<< ln << "." << mate << ".bam";
  else
    path_stream << rt << "/L00" << ln << "/s_"<< ln << "." << mate << ".sam";
  return path_stream.str();
}

uint16_t getSeqCycle(uint16_t cycle, AlignmentSettings* settings, uint16_t read_number) {
	uint16_t seq_cycle = cycle;
	for ( int i = 0; i < read_number; i++ )
		seq_cycle += settings->getSeqById(i).length;
	return seq_cycle;
}

// construct filter file name from: root, lane, tile
std::string filter_name(std::string rt, uint16_t ln, uint16_t tl) {
  std::ostringstream path_stream;
  path_stream << rt << "/L00" << ln << "/s_"<< ln << "_" << tl << ".filter";
  return path_stream.str();
}

// construct position file name from: root, lane, tile
std::string position_name(std::string rt, uint16_t ln, uint16_t tl) {
  std::ostringstream path_stream;
  path_stream << rt << "../L00" << ln << "/s_"<< ln << "_" << tl << ".clocs";
  return path_stream.str();
}

std::string lanePath(uint16_t ln){
	char path [6] = "";
	sprintf(path, "/L%03u", ln);
	return path;
}

std::string bcl_path(std::string rt, uint16_t ln, uint16_t cl) {
  std::ostringstream path_stream;
  path_stream << rt << lanePath(ln) << "/C" << cl << ".1";
  return path_stream.str();
}

// TODO: Is that loop still senseful (e.g. errors during encryption ... )
void copyBcl(AlignmentSettings* settings, std::string original_path, std::string copy_path, uint16_t lane, uint16_t tile, uint16_t cycle, uint16_t read_no, uint16_t delay_sec){

	uint16_t seqCycle = getSeqCycle(cycle, settings, read_no);

	// Delay, if filesystem failure in previous iteration(s)
	if(delay_sec!=1){
		std::this_thread::sleep_for(std::chrono::seconds(delay_sec));
	}

	/* Create the necessary directories for file copies. */
	std::string bcl_dir = bcl_path(copy_path, lane, seqCycle);
//	try {
		boost::filesystem::path dir(bcl_dir);
		boost::filesystem::create_directories(dir);
//	} catch (const boost::filesystem::filesystem_error& err) {
//		delay_sec = std::min(delay_sec*2, 300);
//		std::cerr << bcl_dir << ": Failed to create directories for bcl file copies. Trying again after a delay of " << delay_sec << " seconds. "
//						<< "If this error remains, please make sure to have the required permissions in " << bcl_dir << "." << std::endl;
//				copyBcl(settings, original_path, copy_path, lane, tile, cycle, mate, delay_sec);
//		throw std::runtime_error("Failed to create copy dir: " + bcl_dir);
//	}

	/* Build file paths. */
	std::string bcl_original = bcl_name(original_path, lane, tile, seqCycle);
	std::string bcl_copy = bcl_name(copy_path, lane, tile, seqCycle);

	/* Call the file copy function. */
	if ( !copyFile(settings, bcl_original, bcl_copy) ) {
//		delay_sec = std::min(delay_sec*2, 300);
//		std::cerr << bcl_original << ": Failed to create (encrypted) copy. Trying again after a delay of " << delay_sec << " seconds. "
//				<< "If this error remains, please make sure to have the required permissions in " << copy_path << "." << std::endl;
//		copyBcl(settings, original_path, copy_path, lane, tile, cycle, mate, delay_sec);
		throw std::runtime_error("Failed to copy bcl file: " + bcl_original);
	}

}

bool copyFile(AlignmentSettings* settings, std::string file_in, std::string file_out){

	/* Use filesystem copy if a plain copy is requested. */
	if ( settings->encryptionType==CopyEncryption::PLAIN ) {
		try {
			boost::filesystem::copy_file(file_in, file_out, boost::filesystem::copy_option::overwrite_if_exists);
		} catch (const std::exception &ex) {
			return false;
		}
		return true;
	}

	/* SSL encryption is requested. Resulting file will get a .enc ending. */
	if(settings->encryptionType==CopyEncryption::SSL){
		file_out += ".enc";
		settings->evp_envelope.evp_mutex.lock();
		FILE * in (fopen(file_in.c_str(), "rb"));
		FILE * out (fopen(file_out.c_str(), "wb"));
		if(!in || !out || !encrypt_file_ssl(*settings, in, out)) {
			settings->evp_envelope.evp_mutex.unlock();
			return false;
		}
		fclose(in);
		fclose(out);
		settings->evp_envelope.evp_mutex.unlock();
		return true;
	}

	return false;
}

bool encrypt_file_ssl(AlignmentSettings & settings, FILE * in, FILE * out) {

	/* Load the encryption data structure from the settings. */
	EvpEncryptionData * env = &settings.evp_envelope;

	/* Buffer for in and out stream. */
	unsigned char buffer[1024];
	unsigned char buffer_out[1024 + EVP_MAX_IV_LENGTH];

	/* Size of input data. */
	size_t len;

	/* Size of encrypted output data. */
	int len_out;

	/* Iterate input stream in blocks. */
	while( (len = fread(buffer, 1, sizeof(buffer), in)) > 0 )
	{

		/* Encrypt buffer from input data. */
		if(!EVP_SealUpdate(&env->ctx, buffer_out, &len_out, buffer, len)) {
			fprintf(stderr, "EVP_SealUpdate failed during file encryption. \n");
			return false;
		}

		/* Write encrypted buffer to out stream. */
		if(fwrite(buffer_out, len_out, 1, out) != 1)
		{
			fprintf(stderr, "Writing encrypted file failed. \n");
			return false;
		}
	}

	/* Check for errors in input loop. */
	if(ferror(in))
	{
		fprintf(stderr, "Reading input file during encryption failed. \n");
			return false;
	}

	/* ENcrypt remaining data from the block... */
	if(!EVP_SealFinal(&env->ctx, buffer_out, &len_out))
	{
		fprintf(stderr, "EVP_SealFinal failed during file encryption. \n");
		return false;
	}

	/* ... and write them to output file. */
	if(fwrite(buffer_out, len_out, 1, out) != 1)
	{
		fprintf(stderr, "Writing to file failed during file encryption. \n");
		return false;
	}

	return true;
}

bool init_ssl_encryption(AlignmentSettings & settings){

	// Load data structure for encryption
	EvpEncryptionData * env = &settings.evp_envelope;

	/* Open file containing the public key (must be stored in .pem format). */
	FILE * rsa_pubkey_file ( fopen(settings.pubKeyPath.c_str(), "rb"));

	/* Load public key. */
	if(!PEM_read_RSA_PUBKEY(rsa_pubkey_file, &env->rsa_pubkey, NULL, NULL))
	{
		std::cerr << "Error loading the public key file: " << settings.pubKeyPath << std::endl;
	    ERR_print_errors_fp(stderr);
	    return false;
	}

	/* Give public key to EVP data structure */
	if(!EVP_PKEY_assign_RSA(env->pub_key, env->rsa_pubkey))
	{
		std::cerr << "EVP_PKEY_assign_RSA failed during encryption initialization." << std::endl;
		return false;
	}

	/* Init EVP chiffre-context. */
	EVP_CIPHER_CTX_init(&env->ctx);
	env->encrypted_key = (unsigned char *) malloc(EVP_PKEY_size(env->pub_key));

	 /* Init EVP seal, generate random AES key. */
	if(!EVP_SealInit(&env->ctx, EVP_aes_256_cbc(), &env->encrypted_key, &env->encrypted_key_len, env->iv, &env->pub_key, 1))
	{
		std::cerr << "EVP_SealInit failed during encryption initialization." << std::endl;
		return false;
	}

	/* Write the encrypted key to file. */
	std::string key_path = settings.copy_dir + "/session_key.enc";
	FILE* key_out(fopen(key_path.c_str(), "wb"));
	if(!key_out) {
		std::cerr << "Error writing the encrypted key to file: " << key_path << std::endl;
		return false;
	}
	fwrite(env->encrypted_key, env->encrypted_key_len, 1, key_out);
	fclose(key_out);

	/* Write the initialization vector to file. */
	std::string iv_path = settings.copy_dir + "/session_iv";
	FILE* iv_out(fopen(iv_path.c_str(), "wb"));
	if ( fwrite(env->iv, EVP_aes_256_cbc()->iv_len, 1, iv_out) != 1 ) {
		std::cerr << "Error writing the encrypted initialization vector to file." << std::endl;
		return false;
	}

	fclose(iv_out);

	return true;
}

uint16_t getSeqCycle(uint16_t cycle, uint16_t rlen, uint16_t mate) {
	return (mate - 1) * rlen + cycle;
}

// convert wobble offset to wobble base code
uint8_t getWobbleBase(DiffType wobble){
	switch (wobble){
		case WOBBLE_A: return 0;
		case WOBBLE_C: return 1;
		case WOBBLE_G: return 2;
		case WOBBLE_T: return 3;
		case WOBBLE_DA: return 4;
		case WOBBLE_DC: return 5;
		case WOBBLE_DG: return 6;
		case WOBBLE_DT: return 7;
		case WOBBLE_I: return 8;
		default: return std::numeric_limits<uint8_t>::max();
	}
}

// convert wobble base code to wobble offset
DiffType getWobbleOffset(uint8_t base_id){
	switch (base_id) {
		case 0: return WOBBLE_A;
		case 1: return WOBBLE_C;
		case 2: return WOBBLE_G;
		case 3: return WOBBLE_T;
		case 4: return WOBBLE_DA;
		case 5: return WOBBLE_DC;
		case 6: return WOBBLE_DG;
		case 7: return WOBBLE_DT;
		case 8: return WOBBLE_I;
		default: return 0;
	}
}

// get all kmers with changed based at one position (positions 0-based)
HashIntoType wobbleKmer(HashIntoType kmer, CountType position, uint8_t base) {
	if( base >= 12 || position > K_PriLive)
		return kmer;

	uint8_t bitPosition = 2 * ((uint8_t)position);
	HashIntoType wobbled_kmer;

	// SNP
	if ( base <= 3 ) {
		wobbled_kmer = kmer & ~(3 << bitPosition); // switch the particular bits to 00
		wobbled_kmer |= (base << bitPosition); // insert the particular base
	}

	// DELETION (Deletion in read means that the base must be inserted)
	else if ( base <= 7 ) {
		base = base % 4; // Convert to default base numeration
		wobbled_kmer = kmer & ~(std::numeric_limits<HashIntoType>::max() << (bitPosition + 2)); // keep only the latter part of the k-mer (behind insertion)
		wobbled_kmer |= ( (kmer >> (bitPosition + 2)) << (bitPosition + 4) ); // add the front part
		wobbled_kmer |= (base << (bitPosition + 2)); // insert the base
		wobbled_kmer &= ~(std::numeric_limits<HashIntoType>::max() << 2*K_PriLive); // remove everything in front of the latter 2K bits (K bases)

	}

	// INSERTION (Insertion in Read means that kmer wobble must remove a base)
	else if ( base <= 11 ) {
		base = base % 4;; // Convert to default base numberation
		wobbled_kmer = kmer & ~(std::numeric_limits<HashIntoType>::max() << bitPosition); // keep only the latter part of the k-mer (behind deletion)
		wobbled_kmer |= ( (kmer >> (bitPosition + 2) ) << bitPosition ); // add the front part
		wobbled_kmer |= (base << (2*(K_PriLive-1)) ); // add the base at the first position

	}

	return wobbled_kmer;
}

void split ( const std::string &s, char delim, std::vector<std::string> &elems ) {
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
}
