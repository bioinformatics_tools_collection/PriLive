#ifndef IGNORE_ALN_H
#define IGNORE_ALN_H

#include <alnstream.h>
#include <ignore.h>
#include <kindex.h>
#include <alnread.h>
#include <ign_alnread.h>
#include <parallel.h>
#include <tr1/unordered_map>

/**
 * Class for ignore alignment controlling.
 * Manages the procedure of foreground and background alignment.
 * @author Tobias Loka
 */
class IgnoreAlignmentController{

private:

	/** The base directory of the sequencing run */
	std::string root;

	/** The Alignment settings */
	AlignmentSettings* settings;

public:

	/**
	 * Class constructor of a new IgnoreAlignmentController object. Must only be initialized ONCE!
	 * @param rt The base directory of the sequencing run.
	 * @param set Pointer to an object containing the program settings.
	 * @param lanes List of all considered lanes.
	 * @param tiles List of all considered tiles.
	 * @author Tobias Loka
	 */
	IgnoreAlignmentController(std::string rt, AlignmentSettings* set, std::vector<uint16_t>& lanes, std::vector<uint16_t>& tiles) : root(rt), settings(set){

		if ( !settings->hasIgnoreAlignment() )
			return;

		for ( int mate = 1; mate <= settings->mates; mate++ ) {
			IgnoreHandler ign_handl (0, 0, 0, mate, root, settings, false);

			/* Init cont file */
			if( !ign_handl.init_cont_file() )
				std::cerr << "ERROR: Contamination file " << ign_handl.get_cont_file(settings->temp_dir) << " could not be initialized.";

			/* Init stuff for each lane & tile */
			for(auto ln = lanes.begin(); ln!=lanes.end(); ++ln){
				for(auto tl = tiles.begin(); tl!=tiles.end(); ++tl){
					initIgnoreAlignment(*ln, *tl, mate, settings->getSeqByMate(mate).length);
				}
			}

		}
	}

	/**
	 * Control the consecutive execution of "normal" and ignore alignment.
	 * @param lane The lane of the current task
	 * @param tile The tile of the current task
	 * @param cycle The current read cycle
	 * @param seqEl The read vector entry for the current read
	 * @param idx Pointer to the foreground reference index
	 * @param ign_idx Pointer to the background reference index
	 * @return The number of foreground-alignment related seeds
	 * @author Tobias Loka
	 */
	uint64_t align(Task & t, KixRun* idx, KixRun* ign_idx);

	/**
	 * Init necessary files & data structure for ignore alignment (must be called for each lane-tile-mate combination.
	 * @param lane The respective lane.
	 * @param tile The respective tile.
	 * @param mate The respective mate.
	 * @param rlen The length of the respective mate.
	 * @return true, if successful. false if errors occured.
	 * @author Tobias Loka
	 */
	bool initIgnoreAlignment(uint16_t lane, uint16_t tile, uint16_t mate, uint16_t rlen);

	/**
	 * Coordinate the barcode extension in align files of core and RETIRED algorithm.
	 * @param lane The lane of the current task
	 * @param tile The tile of the current task
	 * @param bc_cycle The cycle of the barcode read.
	 * @param read_no The number of the sequence read for which the barcode will be extended (:= index in AlignmentSettings::seqs).
	 * @param settings Pointer to the object containing the program settings.
	 * @return The number of handled mates (# mates the barcode was extended for). 1 handled mate includes both core and RETIRED alignment.
	 * @author Tobias Loka
	 */
	CountType extend_barcode(uint16_t lane, uint16_t tile, uint16_t bc_cycle, uint16_t read_no, AlignmentSettings * settings);

	/**
	 * Check if bcl modification is activated
	 * @param t Task object containing necessary information for decision (cycle, lane, tile)
	 * @return true if bcl modification is activated
	 */
	bool check_bcl_manipulation();

	/**
	 * Check if the cycle number is correct to discard reads.
	 * @param t Task object containing necessary information for decision (cycle, lane, tile)
	 * @return true if reads get discarded in the current cycle
	 */
	bool is_discard_cycle(Task & t);

};

/**
 * Class for the organization of the background alignment algorithm. Inherits the StreamedAlignment class used for the foreground alignment.
 * @author Tobias Loka
 */
class StreamedIgnoreAlignment : public StreamedAlignment{

public:

	/**
	 * Class constructor of a new StreamedIgnoreAlignment object.
	 * @param ln The respective lane number.
	 * @param tl The respective tile number.
	 * @param rt The base directory of the sequencing run.
	 * @param rl The length of the respective read mate.
	 * @author Tobias Loka
	 */
 	 StreamedIgnoreAlignment(uint16_t ln, uint16_t tl, std::string rt, CountType rl) :
 		 StreamedAlignment(ln, tl, rt, rl){}

 	 /**
 	  * OVERRIDE: Get .ia instead of .align files.
 	  * @param cycle The current cycle of the read mate.
 	  * @param mate The mate number.
 	  * @param base If different base location than root, specify here. [optional]
 	  * @return The full path of the background alignment file.
 	  * @author Tobias Loka
 	  */
 	 std::string get_alignment_file(uint16_t cycle, uint16_t mate, std::string base="");

 	 /**
 	  * OVERRIDE: Extend seeds in the background alignment
 	  * @param cycle The current cycle of the read mate.
 	  * @param read_no The read number of the mate.
 	  * @param mate The mate number.
 	  * @param index The reference index for the background alignment.
 	  * @param settings Pointer to the object containing the program settings.
 	  * @param ignore_handler Pointer to the respective ignore handler.
 	  * @return The number of seeds.
 	  * @author Tobias Loka
 	  */
 	 uint64_t extend_alignment(uint16_t cycle, uint16_t read_no, uint16_t mate, KixRun* index, AlignmentSettings* settings,
 			 std::shared_ptr<IgnoreHandler> ignore_handler = nullptr);

 	 /**
 	  * OVERRIDE: extend barcode within ignore alignment procedure.
 	  * Includes changing the ignore status when the required number of barcodes is available.
 	  * @param bc_cycle The cycle of the barcode read.
 	  * @param read_cycle The last handled cycle for the respective mate (should always be 0 or the full length)
 	  * @param read_no The number of the sequence read for which the barcode will be extended (:= index in AlignmentSettings::seqs).
 	  * @param mate The read mate to extend the barcode.
 	  * @param settings Object containing the program settings.
 	  * @author Tobias Loka
 	  */
 	void extend_barcode(uint16_t bc_cycle, uint16_t read_cycle, uint16_t read_no, uint16_t mate, AlignmentSettings* settings);
};

bool removeFromBcl(BCL_Modification_Job* job, AlignmentSettings* settings, bool removeBarcodes = false);


#endif /* IGNORE_ALN_H */
