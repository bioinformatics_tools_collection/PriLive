![](prilive_logo.png)

Tutorial
-----------

PriLive is a privacy-preserving real-time filtering tool that was designed to remove human reads from Illumina HiSeq (or comparable) data right in the moment when they are produced. This means, read filtering is finished as soon as the sequencer is finished. Human sequence information is never completely available at a single point in time when using PriLive. This enables a new level of genomic privacy for human-related sequencing procedures. PriLive is suitable for genomic and metagenomic data sets.
To perform the examples described in this document, you must download and install PriLive which is available at https://gitlab.com/lokat/PriLive. There you also find the required example data sets. Please follow the instructions described in README.md for the installation of PriLive.
For our examples, we recommend 15GB free disk space, a minimum of 32GB RAM and an installed 64-bit UNIX-like OS. We additionally recommend multithreading with up to 8 cores, but all examples can also be executed with only a single core.
The total time to finish this tutorial will be about 2h on a computer that fulfills the requirements as mentioned above.


Index building
-------

#### Introduction

In general, PriLive makes use of two reference genomes. The foreground reference (FG) is a reference genome of the organism we are actually interested in. All reads that align to FG will be retained. The background reference (BG) is an organism or a collection of sequences that should be removed from the sequencing data. For example, when performing data protection in a viral sequencing procedure, FG is the viral reference sequence and BG is a human reference genome (e.g. human hg38). 
To perform fast read mapping, it is necessary to create a specialized index structure for each of your reference genomes. This must be done only once for each reference genome. The index is stored in .kix (k-mer index) format and must be loaded when using PriLive.
To create (or build) the index of your reference genome, use the executable prilive-build. This file is located in the build directory of PriLive after installation. The general format of prilive-build is as follows:

	>	prilive-build [options] -o ref.kix ref.fa
	
ref.fa is the input reference genome in FASTA format. PriLive supports multi-FASTA format, i.e. FASTA file that contains several sequences. The option -o ref.kix declares the name of the resulting index file. There are only few additional options; the most important of them is the trimming parameter. Trimming is essential for large reference genomes such as the complete human reference genome. The option is used with a natural number N. All k-mers that have a number of occurrences >N in the reference genome are tagged as “trimmed” in the index without storing the actual matching positions. This brings advantages in runtime and index size at the expense of alignment accuracy. Thus, a small trimming value may lead to insufficient alignment quality while a too great value (or no trimming at all) will result in an enormous increase of runtime. As a reference value, we usually trim the complete human reference genome (approx. 3 gbp) with t 1000.

#### Example 1

Our example data sets come with two small reference genomes, cowpox virus Brighton Red (cpxv.fa) and Escherichia coli K-12 (ecoli.fa). Thus, in our examples, we will actually remove E. coli reads from a mixed data set of CPXV and E. coli. For data protection, we would use a human reference genome instead of E. coli.
To build the index files for PriLive, type:

	>	prilive-build -o cpxv.kix cpxv.fa
	>	prilive-build -o ecoli.kix ecoli.fa
	
This will take several minutes. The console output should look similar to this (for CPXV):

	Creating index with K_PriLive=15 from file cpxv.fa
	Writing index to file cpxv.kix
	Number of saved k-mers: 224485.
	
The first line shows the k-mer size that is used for index building. This is declared at compile-time and cannot be changed afterwards. A k-mer size of 15 is suitable for most application cases. When index building is finished, the number of saved k-mers in the index is printed. Because of the design of the index, both index files will be approximately 4GB in size. This is necessary to minimize the access time to each k-mer position during runtime. For a smaller storage size when not using the index files, we recommend to compress the index files with gzip.
Since both reference genomes are pretty small, we don’t make use of the trimming functionality. Both index files are built with the same command. Once built, the resulting index files (.kix) can be used as input for PriLive as foreground or background reference.


Using PriLive
------------

#### Introduction

For genomic data, both index files are used as input for PriLive.
The general syntax of PriLive is as follows:

	>	prilive [options] -f [BG] [BCL_DIR] [FG] [CYCLES] [OUT_DIR] 
	
There is a plethora of options for PriLive such that only the most important ones will be presented in this tutorial. Please have a look into the manual or the help of PriLive for all available options.
The most important algorithmic option is --min-errors (-e).  It describes the number of errors to be tolerated in the foreground alignment. Although this is an option for the foreground alignment it has an enormous impact for the filtering decisions. This is because a plethora of options for the background alignment are computed automatically based on the value of --min-errors. This actually makes sense because, despite being a tool to remove data, the first priority is to NOT remove relevant data and the --min-errors option actually describes what we are interested in: It is the number of mismatches (i.e., single nucleotide substitutions or InDels) that is tolerated in the foreground alignment. Thus, if set to --min-errors 4, all reads that contain up to 4 mismatches should not be removed from the data. This value is not rigorously respected by default such that in a very few special cases a read may be removed despite fulfilling this criteria for the foreground alignment. To prevent this, you can manually set the option --bg-discard to [--min-errors] + 1.   
The second algorithmic option that is relevant for this tutorial is --bg-score. This option describes the minimal alignment score to the background reference that must be reached to remove a read. The alignment score is actually the number of matching base pairs of the respective local alignment to the background reference. By default, this option is set automatically considering different technical aspects (e.g. read length and k-mer size) as well as the --min-errors parameter that was set by the user. While the --min-errors option should always be set manually depending on different factors as, for example, the read length or the expected mutation rate of the foreground reference, the --bg-score option mainly comes into play when no foreground reference is used. This can be the case for instance for metagenomic data. For such data, --min-errors should always be set to 0 such that no foreground alignment is considered for read filtering. The balance of specificity and sensitivity for the filtering (background alignment) is then only controlled by --bg-score. Because of the concept for the background alignment we strongly recommend to always choose a value that is lower than half of the read length.   
Besides the algorithmic options, it is usually necessary to set several technical values. These include --tiles (-t), --lanes (-l) and --temp that declare the tiles and lanes that are handled by PriLive as well as the directory for temporary files, respectively. The number of alignment threads can be set with the --num-threads (-n) option. Thereby, PriLive only supports a number of threads as high as the number of tiles multiplied by the number of lanes.
One of the most important technical setups is the definition of the sequence structure, i.e. the order and length of reads and barcodes, as well as the barcode sequences for real-time demultiplexing. If these options are not set correctly, this can lead to a lack of privacy (in data protection applications) or the loss of relevant data. The order and length of the sequences can be set by the --reads (-r) option. Thereby, the single sequences contain information about their length and the sequence type (R=read, B=barcode). The sequences are delimited by a whitespace. As an example, a paired-end sequencing procedure with the structure 101-8-8-101 (which means 2x101bp reads and a dual barcode of length 2x8bp in between both reads) is described by --reads 101R 8B 8B 101R. The total length of all sequences must sum up to the number of cycles which is an obligatory parameter of PriLive ([CYCLES]). For the example above, the number of cycles is 218. The barcode sequences can be defined with the --barcode option. Thereby, the different parts of a barcode are delimited by a “-“ while several different barcodes are delimited by a whitespace. Thus, two different barcodes for the scenario described above (2x8bp) can be defined as follows: --barcodes ATCTGGCT-ATCGAATC GGATGCTA-CGGCTAGC. By default, 1 error is tolerated for each part of a barcode. This can be adapted by the --barcode-errors (-E) parameter, e.g. --barcode-errors 2 3 to tolerate 2 errors for the first and 3 errors for the second part of a barcode. If barcodes are declared in --reads but the filtering should be performed without considering the barcode information, add the --keep-all-barcodes to disregard the barcode sequences.

#### Example 2

Before starting this example, please build the index files for PriLive as described in Example 1.

Additionally, you need to decompress the BaseCalls directory:

	>	tar zxf BaseCalls.tar.gz

For this example, cowpox virus Brighton Red (CPXV) will serve as foreground reference while we aim at removing all reads of Escherichia coli (E. coli).
The example data set contains 3,000 reads for both organisms with an error rate of 0, 3 and 6 percent (1,000 reads each). Each of the resulting six subsamples has its own lane (CPXV: L001-L003; E. coli: L004-L006) and a single tile.

###### Genomic data (Single End)

For this example, we assume to know the expected foreground reference (CPXV). Thus, the CPXV index that was built in Example 1 will serve as a foreground reference genome. The E. coli index will be used as background reference genome. 
Besides technical settings and the number of errors, all other options can generally remain at their default values. Thus, we use the following command:

	>	prilive --lanes 1 2 3 4 5 6 --tiles 1101 --temp ./temp -f ecoli.kix --bg-freq 0 -e 3 --accept-disclaimer -n 6 ./BaseCalls cpxv.kix 100 out

Besides the options that were described in the introduction, you may recognize the --bg-freq 0 and --accept-disclaimer options. --bg-freq 0 ensures that the base call files are not modified because the same base call files are used several times in our examples. For privacy applications, we strongly recommend to use the default value or to set this option to a value other than 0. By setting --accept-disclaimer you confirm that you agree with the disclaimer of PriLive. We strongly advise you to read it before using PriLive (even for these examples). If this parameter is not set, you will be prompted to agree with the disclaimer manually by typing “yes” when you start PriLive. All other options are used as described above: we have 6 lanes with 1 tile each (tile number 1101). The number of tolerated errors for the foreground alignment is 3 and the read length is 100bp. We executed PriLive with 6 alignment threads.
At the beginning, PriLive prints an overview of the most important parameters that were either set by the user or computed automatically. The further command line output of PriLive shows the number of existing seeds for each lane and tile. Thereby, the first number is the number of seeds for the foreground alignment, the second number for the background alignment. The numbers are important to recognize tiles with no or only few valid reads, e.g. due to bad quality or sequencing faults (both numbers close to 0). Additionally, the numbers can indicate whether the expected foreground and background organisms are actually present in the data or not (one of both numbers close to 0). For the example data you will recognize that for most tiles one of both numbers is (nearly) 0 since the reads of both organisms are on different lanes.
Finally, PriLive produces several output files of interest. First, have a look at the foreground alignment results. You find them in out/L00*. To check your results, type:

	>	cat out/L00*/*.sam.stats
	
The CPXV reads on the lanes L001-L003 (the first 6 lines of the command output) should have valid alignments with approximately 1000, 620 and 141 aligned reads respectively. For the E. coli reads on the lanes L004-L006, there should be no foreground alignments.
The second output file of interest is out/filtered.cont. In this file, you find a list of reads that were detected as background data by PriLive. Here, you should find as many E. coli reads and as few CPXV reads as possible:

	>	grep "cpxv" out/filtered.cont | wc -l
	>	grep "coli" out/filtered.cont | wc -l
	
While the first command should return 0 showing that no CPXV reads have been filtered, approximately 2,771 E. coli reads are detected by PriLive as you can see by executing the second command. The remaining 236 E. coli reads are not detected because of the high error rates for the simulated reads (up to 6%).

###### Demultiplexing

In total, our example data set contains paired-end sequencing data of length 2x100bp and 8bp barcode sequence. In the example above, we only used the first of both reads. The barcode sequence was not considered. All reads of the first six lanes have the same barcode sequence (ACGTACGT). Our data set contains two additional lanes to demonstrate the demultiplexing functionality. These two lanes L007 and L008 contain the same sequence information as lane L001 and L004, respectively. These are the error-free reads for both organisms. All reads of these two lanes have the same barcode sequence with three errors when compared to the original barcode sequence (AGCTAGGT). In this part of the example, we will show how to use live demultiplexing for single and dual barcodes.
At first, we assume the 8bp barcode sequence to be a single barcode and still only consider the first read:

	>	prilive --lanes 1 2 3 4 5 6 7 8 --tiles 1101 --temp ./temp -f ecoli.kix --bg-freq 0 -e 3 -r 100R 8B -b ACGTACGT --accept-disclaimer -n 8 ./BaseCalls cpxv.kix 108 out

With -r 100R 8B we define the general structure of the sequencing procedure. In this case, it is a read of 100bp length at the beginning and a barcode of length 8bp at the end. Therefore, we have to increase the total number of cycles to 108. The barcode sequence is defined by -b ACGTACGT.
For the lanes L001-L006, which have the correct barcode, we have the same results as in the example before without using barcode information. For the lanes L007 and L008, despite having error-free reads for CPXV (L007) and E. coli (L008), we have neither any foreground mapping for lane L007 nor any filtered reads for lane L008:

	>	cat out/L00*/*.sam.stats
	>	grep "wrongBC" out/filtered.cont | wc -l

By default, one substitution per barcode (fragment) is permitted. Thus, when increasing the tolerated number of substitutions for the barcode, the reads on lane L007 and L008 should be considered:

	>	prilive --lanes 1 2 3 4 5 6 7 8 --tiles 1101 --temp ./temp -f ecoli.kix --bg-freq 0 -e 3 -r 100R 8B -E 3 -b ACGTACGT --accept-disclaimer -n 8 ./BaseCalls cpxv.kix 108 out
	>	cat out/L00*/*.sam.stats
	>	grep "wrongBC" out/filtered.cont | wc -l
	
We could also interpret the barcode sequence as dual barcode of length 2x4bp. To tolerate one mismatch for each barcode fragment, we can just set E 1 (default). The barcode sequences must be delimited with a “-“. Then, for the lanes L007 and L008, no reads will be aligned or filtered because of too many errors in the barcode sequence:

	>	prilive --lanes 1 2 3 4 5 6 7 8 --tiles 1101 --temp ./temp -f ecoli.kix --bg-freq 0 -e 3 -r 100R 4B 4B -b ACGT-ACGT --accept-disclaimer -n 8 ./BaseCalls cpxv.kix 108 out
	>	cat out/L00*/*.sam.stats
	>	grep "wrongBC" out/filtered.cont | wc -l
	
Alternatively, -E can also be used to define the tolerated number of errors for each barcode fragment individually:

	>	prilive --lanes 1 2 3 4 5 6 7 8 --tiles 1101 --temp ./temp -f ecoli.kix --bg-freq 0 -e 3 E 2 1 -r 100R 4B 4B -b ACGT-ACGT --accept-disclaimer -n 8 ./BaseCalls cpxv.kix 108 out
	>	cat out/L00*/*.sam.stats
	>	grep "wrongBC" out/filtered.cont | wc -l
	
In the last example, the tolerated number of error for each barcode is exactly as the barcodes were designed, thus the reads of the lanes L007 and L008 should be aligned / filtered again.

###### Paired-end sequencing

Before performing this example, please clean up the output folder to remove the results of the previous examples:

	>	rm -r ./out
	
Using PriLive for paired-end sequencing data is closely linked to the live demultiplexing functionality as described above. Thus, paired-end reads are also defined by using the r option. Our data set contains paired-end reads of 2x100bp with an 8bp barcode sequence in between. The resulting command looks as follows:

	>	prilive --lanes 1 2 3 4 5 6 --tiles 1101 --temp ./temp -f ecoli.kix --bg-freq 0 -e 3 -r 100R 8B 100R --accept-disclaimer --keep-all-barcodes -n 6 ./BaseCalls cpxv.kix 208 out

With the keep-all-barcodes option, the barcode information is not used, thus all reads are considered regardless their barcode sequence. Two alignment output files for the foreground alignment are written for each tile, one for each of both reads. For example, for lane L002, type the following command to see the statistics for both reads:

	>	cat out/L002/*.1.sam.stats
	>	cat out/L002/*.2.sam.stats
	
As you can see, both reads are aligned completely independently. If you are only interested in alignments of both reads, you have to preprocess the results from PriLive. For the background alignment, this is completely different since a read pair is classified as background-related if one of both reads fulfills the criteria. Thus we should observe a slightly increased number of filtered reads in total compared to the results of the single-end examples:

	>	grep "ecoli" out/filtered.cont | wc -l

Here, we observe 2,950 filtered E. coli reads compared to 2,771 that have been detected when only using the sequence information of the first read.

###### Metagenomic data

PriLive is also perfectly suitable for the use with metagenomic data. Here, we use PriLive without a foreground reference genome. Instead, we have to adapt at least two parameters to achieve good results. The first one is as before the -e parameter which should be set to 0 when not using a foreground reference genome. This is to guarantee that the complete sequence information is used for the background alignment algorithm. The second option that must be adapted is the --bg-score. This value strongly influences the balance of sensitivity and specificity. In general, we recommend to set this value slightly below half of the read length of a single read. For this example, we chose length 45 for the read of length 2x100bp. As mentioned before, no foreground reference genome is used for metagenomic data. However, the foreground reference genome is an obligatory parameter is PriLive (this is done to guarantee command compatibility with HiLive). Thus, we have to use a “null reference” as foreground reference genome which contains no meaningful sequence information at all. You can use the example file null.fa to create such an index:

	>	prilive-build -o null.kix null.fa

Once you have done this, you can run our metagenomic example with the following command:

	>	prilive --lanes 1 2 3 4 5 6 --tiles 1101 --temp ./temp -f ecoli.kix --bg-freq 0 -e 0 --bg-score 45 -r 100R 8B 100R --accept-disclaimer --keep-all-barcodes -n 6 ./BaseCalls null.kix 208 out
	
You can see that there are no foreground alignments at all:

	>	cat out/L00*/*.1.sam.stats
	>	cat out/L00*/*.2.sam.stats

In the filtered data, there are 2,891 filtered reads compared to 2,950 filtered reads in the results of the paired-end example for genomic data (using the CPXV index as foreground reference genome):

	>	grep "ecoli" out/filtered.cont | wc -l

The sensitivity is slightly worse for these data because of the higher value for --bg-score which guarantees a high specificity at the expense of some sensitivity: Just as in the example for genomic data, no single CPXV read was filtered:

	>	grep "cpxv" out/filtered.cont | wc -l
	
###### Copy mode and encryption

To use PriLive without irrecoverably removing the original sequencing data, it provides a copy mode where all original base call files are copied before any information is removed from the data. By default, if activating the copy mode by providing a copy directory with --copy COPYDIR, SSL encryption is activated. This requires that an RSA key pair is created beforehand. The copy mode can also be activated without encryption of the data (see below; this is NOT recommended for privacy applications).
While the SSL encryption itself is completely included in PriLive, openSSL is required to be installed to create a key pair and to decrypt the data using the provided script decrypt_bcl_copies.sh. To create a private key (e.g. 2048 bits), type:

	>	openssl genpkey -algorithm RSA -out private_key.pem -pkeyopt rsa_keygen_bits:2048

THE RESULTING FILE private_key.pem CONTAINS THE PRIVATE KEY, MUST BE STORED SECURELY AND MUST NEVER BE HANDED OVER TO INDIVIDUALS THAT ARE NOT PERMITTED TO HAVE UNLIMITED ACCESS TO ALL SEQUENCING DATA! In general, we recommend to hand over the private key to a data protection officer who in turn does not have access to the encrypted data. Only if a decryption of certain data is unavoidable for some reason (and granted by the highest instance), the specific data are given to the data protection officer who decrypts the data and returns the decrypted results (potentially after an additional filtering instance) to the researchers.
The private key is used to create the public key with the following command:

	>	openssl rsa -pubout -in private_key.pem -out public_key.pem

The file public_key.pem contains the public key which must be handed over to the sequencing facility or individuals that apply PriLive during the sequencing procedure. This public key is given to PriLive for encryption. However, the data themselves are not directly encrypted by this key. Instead, a hybrid encryption approach is used where a symmetric encryption is performed with an individual key for each sequencing run. The key for this symmetric encryption is encrypted by the public key that was created above and can only be decrypted with the private key.
The PriLive command to run the copy mode with hybrid encryption using the public key that was created above can look like this:

	>	prilive --lanes 1 2 3 4 5 6 --tiles 1101 --temp ./temp -f ecoli.kix --bg-freq 0 --copy ./copy_enc --pub-key public_key.pem -e 0 --bg-score 45 -r 100R 8B 100R --accept-disclaimer --keep-all-barcodes -n 6 ./BaseCalls null.kix 208 out

The encrypted base call files can be decrypted with the decrypt_bcl_copies.sh script: 

	>	decrypt_bcl_copies.sh ./copy ./copy/session_key.enc ./copy/session_iv ./private_key.pem
	
The decrypted base call files are in the same folder as the respective encrypted files.
If encryption is not required, it can be deactivated with --copy-enc PLAIN:

	>	prilive --lanes 1 2 3 4 5 6 --tiles 1101 --temp ./temp -f ecoli.kix --bg-freq 0 --copy ./copy --copy-enc PLAIN -e 0 --bg-score 45 -r 100R 8B 100R --accept-disclaimer --keep-all-barcodes -n 6 ./BaseCalls null.kix 208 out

Please note that the encryption functionality provided by PriLive does not guarantee a sufficient level of security against targeted attacks by specialists but is rather designed to prevent easy access for normal users.